from abc import abstractmethod, ABC
from typing import List, Optional

from wert14_service_geocoding.messages import ParsedAddress

end_add = "e"  # house number addition if none are given for a house number range


class BaseAddressSplitter(ABC):
    def __init__(self, max_results: int, with_adds: bool):
        self.max_results = max_results
        self.with_adds = with_adds  # set to true if adds should be generated

    def contains_number(self, string: str) -> bool:
        """
        Used to check if a given input string contains a digit

        Parameters
        ----------
        string
            The input string to check for digits

        Returns
        -------
        ...
            True if the string contains a digit, False otherwise
        """
        return any(char.isdigit() for char in string)

    @abstractmethod
    async def split(self, parsed_address: ParsedAddress) -> List[ParsedAddress]:
        """
        Abstract method to ensure a split method is present in all subclasses
        """
        pass

    def create_range(
        self, num1: int, add1: Optional[str], num2: Optional[int], add2: Optional[str]
    ) -> List[str]:
        if (
            int(num1) > 2000 or int(num2) > 2000
        ):  # simple solution to prevent zip codes from being processed
            return []
        if (
            not add1 and not add2
        ):  # this is executed when only numbers without adds are found
            if self.with_adds:
                return self.__create_address_range_with_adds(num1, "a", num2, end_add)
            else:
                return list(range(int(num1), int(num2) + 1))
        elif not add2:
            add2 = add1  # assume that the given add is part of the second address
            add1 = "a"  # start at 'a' and create a range until the given add
            return self.__create_address_range_with_adds(num1, add1, num2, add2)

        elif not num2:
            num2 = num1  #

            return self.__create_address_range_with_adds(num1, add1, num2, add2)
        else:
            house_number_list = list(range(int(num1), int(num2) + 1))
            additions = list(range(ord(add1), ord(add2) + 1))
            full_range = []
            for element in house_number_list:
                if element not in full_range:
                    full_range.append(element)
                for add in additions:
                    full_range.append(str(f"{element}{chr(add)}"))
            return full_range

    def __create_address_range_with_adds(self, num1, add1, num2, add2) -> List[str]:
        house_numbers = []
        additions = list(
            range(ord(add1), ord(add2) + 1)
        )  # create a range of expected adds
        numbers = list(range(int(num1), int(num2)))
        for num in numbers:
            house_numbers.append(num)
            for add in additions:
                house_numbers.append(f"{num}{chr(add)}")
        return house_numbers

    def create_parsed_address_array(
        self, house_number_array: List[str], parsed_address: ParsedAddress
    ) -> List[ParsedAddress]:
        """
        Reformats results into ParsedAddress objects

        Parameters
        ----------
        house_number_array
            A list of found house numbers

        parsed_address
            The input ParsedAddress containing all address information

        Returns
        -------
        parsed_address_array
            An array of generated ParsedAddress objects with their individual house numbers
        """
        house_number_array = list(dict.fromkeys(house_number_array))
        parsed_address_array = [parsed_address]
        for house_number in house_number_array:
            if house_number != parsed_address.house_number:
                parsed_address_array.append(
                    ParsedAddress(
                        road=parsed_address.road,
                        house_number=house_number,
                        postcode=parsed_address.postcode,
                        city=parsed_address.city,
                        country=parsed_address.country,
                        state=parsed_address.state,
                        city_district=parsed_address.city_district,
                        potential_district=parsed_address.potential_district,
                    )
                )
        return list(parsed_address_array)
