import json
from pathlib import Path

import pytest
from aiohttp import ClientSession

from wert14_service_geocoding.processing.address_parser.wert14_libpostal import (
    Wert14LibPostal,
)
from wert14_service_geocoding.processing.address_rows.nltk_address_splitter import (
    NLTKAddressSplitter,
)
from wert14_service_geocoding.processing.address_rows.regex_address_splitter import (
    RegexAddressSplitter,
)
from wert14_service_geocoding.rules import Rules


def get_fixture() -> Path:
    return Path(__file__).parent / "fixtures" / "address_rows.json"


async def create_input_array(name):
    async with ClientSession() as session:
        parser = Wert14LibPostal(rules=Rules(), http_client=session)
        parsed_addresses = await parser.parse_address(name)
        return parsed_addresses


def load_json_data():
    with open(get_fixture()) as file:
        content = json.load(file)
        return content


@pytest.fixture
def get_test_data():  # get all name attributes from test file
    names = []
    expected = []
    for item in load_json_data():
        names.append(item["input_address"])
        expected.append(item["expected"])
    return names, expected


@pytest.mark.asyncio
@pytest.mark.online
async def test_create_house_number_array(get_test_data):
    splitter = RegexAddressSplitter(
        max_results=100, with_adds=False
    )  # max_results set to high amount so test runs properly
    await split_routine(get_test_data, splitter)


@pytest.mark.asyncio
@pytest.mark.online
async def test_nltk_split(get_test_data):
    splitter = NLTKAddressSplitter(max_results=100, with_adds=False)
    await split_routine(get_test_data, splitter)


async def split_routine(get_test_data, splitter):
    names_list, expected_list = get_test_data
    for name, expected in zip(names_list, expected_list):
        parsed_name = await create_input_array(name)
        individual_house_numbers = await splitter.split(parsed_name[0])
        i = 0
        for item in individual_house_numbers:
            assert item.house_number == expected[i]
            i += 1
