import re
from typing import List
from wert14_service_geocoding.messages import ParsedAddress
from wert14_service_geocoding.processing.address_rows.base_address_splitter import (
    BaseAddressSplitter,
)


class RegexAddressSplitter(BaseAddressSplitter):
    async def __extract_address_values(
        self, parsed_addresses: ParsedAddress
    ) -> List[str]:
        """
        Checks a LibPostal parsed address and extracts the house_number from either the house_number or road attribute,
        then calls a function to look for patterns

        Parameters
        ----------
        parsed_addresses
            LibPostal output for the given input

        Returns
        -------
        house_number_matches
            A list of all pattern matches
        """
        house_number_matches = []
        if parsed_addresses.house_number:
            house_number_matches = self.__filter_individual_addresses(
                parsed_addresses.house_number.replace("&", "+")
                .replace("und", "+")
                .replace("u.", "+")
                .replace(",", "+")
                .replace("/", "+")
                .replace("bis", "-")
                .replace(" ", "")
                #  standardize the input
            )

        elif parsed_addresses.road:
            house_number_matches = self.__filter_individual_addresses(
                parsed_addresses.road.replace("&", "+")
                .replace("und", "+")
                .replace("u.", "+")
                .replace(",", "+")
                .replace("/", "+")
                .replace("bis", "-")
                .replace(" ", "")
            )
        return house_number_matches

    async def split(self, parsed_address: ParsedAddress) -> List[ParsedAddress]:
        """
        General routine to preprocess a given address string

        Parameters
        ----------
        parsed_address
            String to search for possible address rows

        Returns
        -------
        ...
            A list of all determined addresses contained in the input string. This is cast to a string to work with the
            test data and may be changed later
        """
        matches = await self.__extract_address_values(parsed_address)
        individual_addresses = []
        if bool(re.search(r"[+]", "".join(matches))):
            matches = list(filter("+".__ne__, matches))
            last_digit = 0
            for element in matches:
                if len(matches) > 2:
                    if all(char.isdigit() for char in element):
                        last_digit = element
                        individual_addresses.append(element)
                    elif all(char.isalpha() for char in element):
                        individual_addresses.append(f"{last_digit}{element}")
                    else:
                        individual_addresses.append(element)
                else:
                    individual_addresses.append(element)

        if bool(re.search(r"-", entry) for entry in matches):
            try:
                matches.remove("-")
                if len(matches) == 2:  # 1-3
                    individual_addresses.append(
                        self.create_range(int(matches[0]), None, int(matches[1]), None)
                    )
                elif len(matches) == 3:
                    if matches[1].isdigit():
                        individual_addresses.append(
                            self.create_range(
                                int(matches[0]), "a", int(matches[1]), matches[2]
                            )
                        )
                    else:
                        individual_addresses.append(
                            self.create_range(
                                int(matches[0]), matches[1], int(matches[0]), matches[2]
                            )
                        )
                elif len(matches) == 4:
                    individual_addresses.append(
                        self.create_range(
                            int(matches[0]), matches[1], int(matches[2]), matches[3]
                        )
                    )
                individual_addresses = individual_addresses[0]
            except:
                pass

        if len(individual_addresses) > self.max_results:
            return []  # return empty list if too many results are found
        return self.create_parsed_address_array(individual_addresses, parsed_address)

    async def __is_address_range(self, parsed_address: ParsedAddress) -> bool:
        """
        Check if multiple house numbers are contained in the address string

        Parameters
        ----------
        parsed_address
            The input string to check for possible address rows

        Returns
        -------
        Bool
            True if multiple house numbers are found, False otherwise
        """
        house_number_matches = await self.__extract_address_values(parsed_address)
        if len(house_number_matches) <= 1:
            return False
        return True

    def __filter_individual_addresses(self, house_number: str) -> List[str]:
        """
        Returns a list of processable patterns from a house_number

        Parameters
        ----------
        house_number
            The house number LibPostal output to search for patterns

        Returns
        -------
        ...
            A list of all found matches
        """
        # extract every unique house number from the input string
        return re.findall(
            r"([\d]+|[a-zA-Z]+|[-+])",
            #  Any number followed by any amount of whitespaces, letters or special signs
            #  Output should be something like ['1a-3b']
            house_number,
        )
