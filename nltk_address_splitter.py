from typing import List, Tuple
from wert14_service_geocoding.messages import ParsedAddress
from wert14_service_geocoding.processing.address_rows.base_address_splitter import (
    BaseAddressSplitter,
)
import nltk

num_tags = ["LS", "CD"]
add_tags = ["NN", "DT", "LS", "JJ", "$"]
and_symbols = ["u.", "und", "&", "+", ",", "/"]
to_symbols = ["-", "bis", ":"]


class NLTKAddressSplitter(BaseAddressSplitter):
    async def split(self, parsed_address: ParsedAddress) -> List[ParsedAddress]:
        tokens = []
        if parsed_address.house_number:
            tokens = self.__tokenize_chars(parsed_address.house_number)
        elif parsed_address.road:
            tokens = self.__tokenize_chars(parsed_address.road)
        pos_tags = nltk.pos_tag(tokens)
        hn_array = []
        nums, adds = self.extract_house_numbers(pos_tags)
        if any(element in to_symbols for element in tokens):
            if adds:
                if len(nums) == 1:
                    nums.append(nums[0])
                if len(adds) == 1:
                    adds.insert(0, "a")
                hn_array = self.create_range(
                    int(nums[0]), adds[0], int(nums[1]), adds[1]
                )
            elif all(num.isnumeric() for num in nums):
                hn_array = self.create_range(int(nums[0]), None, int(nums[1]), None)

        if any(element in and_symbols for element in tokens):
            for num in nums:
                hn_array.append(num)
                if adds:
                    for add in adds:
                        hn_array.append(num + add)
                else:
                    hn_array.append(num)
        return self.create_parsed_address_array(hn_array, parsed_address)

    def __tokenize_chars(self, user_input: str) -> List[str]:
        """
        Generates a token for each char in the input string, effectively isolating each char as a list element
        """
        i = 0
        tmp = []
        for char in (
            user_input.replace("&", "+")
            .replace("und", "+")
            .replace("u.", "+")
            .replace(",", "+")
            .replace("/", "+")
            .replace(" ", "")
        ):  # filter indices of non-digits
            if not char.isdigit():
                tmp.append(i)
            i += 1
        user_input = list(user_input)
        tmp = list(reversed(tmp))
        for index, l in enumerate(tmp):
            user_input.insert(l + 1, " ")
            user_input.insert(
                l, " "
            )  # add whitespace before and after the non-digit to isolate number inputs
        sanitized = "".join(user_input)
        tokens = nltk.word_tokenize(
            sanitized
        )  # check each individual symbol of the input
        return tokens

    def extract_house_numbers(self, pos_tags) -> Tuple[List[str], List[str]]:
        """
        Used to identify numbers and additions in a given Tuple of words and pos_tags
        """
        nums = []
        adds = []
        for item in pos_tags:
            if item[1] in num_tags:
                nums.append(item[0])
            elif (
                item[1] in add_tags
                and not item[0] in and_symbols
                and not item[0] in to_symbols
            ):
                adds.append(item[0])
        for num in nums:
            if not num.isdigit():
                nums.remove(num)
        return nums, adds
